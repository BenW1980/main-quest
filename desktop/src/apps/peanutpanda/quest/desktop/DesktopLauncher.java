package apps.peanutpanda.quest.desktop;

import apps.peanutpanda.quest.Quest;
import apps.peanutpanda.quest.states.ScreenSize;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
    public static void main(String[] arg) {

        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        System.setProperty("org.lwjgl.opengl.Window.undecorated", "false");

        config.title = "Main Quest";
        config.width = ScreenSize.WIDTH.getSize() * 3;
        config.height = ScreenSize.HEIGHT.getSize() * 3;
//        config.width = LwjglApplicationConfiguration.getDesktopDisplayMode().width;
//        config.height = LwjglApplicationConfiguration.getDesktopDisplayMode().height;
//        config.fullscreen = true;
        config.resizable = true;
        new LwjglApplication(new Quest(), config);
        

    }
}