package apps.peanutpanda.quest;

import apps.peanutpanda.quest.assetmanager.AssetLoader;
import apps.peanutpanda.quest.controller.KeyBoard;
import apps.peanutpanda.quest.controller.Mouse;
import apps.peanutpanda.quest.gameobjects.*;
import apps.peanutpanda.quest.itemhandling.ItemHandler;
import apps.peanutpanda.quest.light.Clock;
import apps.peanutpanda.quest.light.LightRenderer;
import apps.peanutpanda.quest.screens.*;
import apps.peanutpanda.quest.states.*;
import apps.peanutpanda.quest.ui.Command;
import apps.peanutpanda.quest.ui.ConversationGrid;
import apps.peanutpanda.quest.ui.OutputDialog;
import apps.peanutpanda.quest.ui.VerbGrid;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

@SuppressWarnings("LibGDXUnsafeIterator")
public class Quest extends Game {

    public AssetLoader assLoader;

    public OrthographicCamera camera;
    public Viewport viewport;
    public KeyBoard keyBoard;
    public Mouse mouse;
    public BitmapFont fontHoverText;
    public BitmapFont fontVerbs;
    public BitmapFont fontOutputDialog;
    public Flag flag;
    public Stage stage;

    private Array<MapItem> mapItems;
    private Array<MapInventoryItem> mapInventoryItems;
    private Array<AnimatedItem> animatedItems;
    private Array<NPC> npcs;
    public Array<Sprite> invBtnBackground;

    public GameState gameState;

    public ItemHandler itemHandler;

    public float invBtnBackgrAlpha = 0.3f;
    public Inventory inventory;
    public Command command;
    public OutputDialog outputDialog;
    public Clock clock;
    public LightRenderer lightRenderer;

    public VerbGrid verbGrid;
    public ConversationGrid conversationGrid;
    public Zone zone;


    @Override
    @SuppressWarnings("unchecked")
    public void create() {
        this.assLoader = new AssetLoader();
        this.assLoader.loadFontHandling();
        this.assLoader.loadTiledMaps();
        this.assLoader.loadImages();
        this.assLoader.loadAtlas();
        this.assLoader.loadSkins();
        this.assLoader.loadMusic();
        this.assLoader.manager.finishLoading();

        this.keyBoard = new KeyBoard();
        this.flag = new Flag();

        this.fontOutputDialog = assLoader.manager.get("monkey20.ttf", BitmapFont.class);
        this.fontOutputDialog.getData().setScale(0.2f, 0.24f);
        this.fontOutputDialog.setUseIntegerPositions(false);

        this.fontHoverText = assLoader.manager.get("monkey35.ttf", BitmapFont.class);
        this.fontHoverText.getData().setScale(0.2f, 0.24f);
        this.fontHoverText.setUseIntegerPositions(false);

        this.fontVerbs = assLoader.manager.get("monkey50.ttf", BitmapFont.class);
        this.fontVerbs.getData().setScale(0.2f, 0.24f);
        this.fontVerbs.setUseIntegerPositions(false);

        this.camera = new OrthographicCamera();
        this.camera.setToOrtho(false, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        this.viewport = new FitViewport(ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize(), camera);
        this.stage = new Stage(viewport);
        this.camera.update();
        Json json = new Json();
        this.mapItems = json.fromJson(Array.class, Gdx.files.internal("json/mapItems.json"));
        this.mapInventoryItems = json.fromJson(Array.class, Gdx.files.internal("json/mapInventoryItems.json"));
        this.animatedItems = json.fromJson(Array.class, Gdx.files.internal("json/animatedItems.json"));
        this.npcs = json.fromJson(Array.class, Gdx.files.internal("json/npc.json"));

        Array<Array<? extends Entity>> allEntitiesFromJson = new Array<>();
        allEntitiesFromJson.add(mapItems, mapInventoryItems, animatedItems, npcs);

        this.itemHandler = new ItemHandler(assLoader, allEntitiesFromJson);

        this.lightRenderer = new LightRenderer(camera);
        this.clock = new Clock(assLoader, stage);
        this.mouse = new Mouse();

        InputMultiplexer multiplexer = new InputMultiplexer();
        Gdx.input.setInputProcessor(multiplexer);
        multiplexer.addProcessor(this.keyBoard);
        multiplexer.addProcessor(this.mouse);
        multiplexer.addProcessor(this.stage);

        this.command = new Command();
        this.outputDialog = new OutputDialog(fontHoverText, stage, assLoader, mouse, command);

        this.invBtnBackground = new Array<>();

        for (int i = 0; i < 8; i++) {
            this.invBtnBackground.add(new Sprite(assLoader.invBtnBack()));
            this.invBtnBackground.get(i).setAlpha(invBtnBackgrAlpha);
        }

        int widthBtn = 52;
        int heightBtn = 28;
        int y = 36;

        this.invBtnBackground.get(0).setBounds(stage.getWidth() / 2 + 15, y - 2, widthBtn, heightBtn);
        this.invBtnBackground.get(1).setBounds(stage.getWidth() / 2 + 70, y - 2, widthBtn, heightBtn);
        this.invBtnBackground.get(2).setBounds(stage.getWidth() / 2 + 125, y - 2, widthBtn, heightBtn);
        this.invBtnBackground.get(3).setBounds(stage.getWidth() / 2 + 180, y - 2, widthBtn, heightBtn);

        this.invBtnBackground.get(4).setBounds(stage.getWidth() / 2 + 15, y - 32, widthBtn, heightBtn);
        this.invBtnBackground.get(5).setBounds(stage.getWidth() / 2 + 70, y - 32, widthBtn, heightBtn);
        this.invBtnBackground.get(6).setBounds(stage.getWidth() / 2 + 125, y - 32, widthBtn, heightBtn);
        this.invBtnBackground.get(7).setBounds(stage.getWidth() / 2 + 180, y - 32, widthBtn, heightBtn);

        this.inventory = new Inventory(invBtnBackground, stage, command);

        inventory.add(itemHandler.book);
        inventory.add(itemHandler.torch);

        this.verbGrid = new VerbGrid(command, fontVerbs);
        this.conversationGrid = new ConversationGrid(fontOutputDialog, stage);

        this.gameState = GameState.IN_GAME;
        this.changeScreen(ScreenState.FARM);

    }

    public NPC createNPC(String id, float x, float y) {

        for (NPC npc : npcs) {
            if (npc.getId().equals(id)) {
                npc.create(assLoader, x, y);
                return npc;
            }
        }
        return null;
    }

    public MapItem createMapItem(String id, TiledMap tiledMap) {

        for (MapItem item : mapItems) {
            if (item.getId().equals(id)) {
                item.create(tiledMap);
                return item;
            }
        }
        return null;
    }

    public MapInventoryItem createMapInventoryItem(String id, TiledMap tiledMap) {

        for (MapInventoryItem item : mapInventoryItems) {
            if (item.getId().equals(id)) {
                item.create(assLoader, tiledMap);
                return item;
            }
        }
        return null;
    }

    public AnimatedItem createAnimatedItem(String id, TiledMap tiledMap) {

        for (AnimatedItem item : animatedItems) {
            if (item.getId().equals(id)) {
                item.create(tiledMap, assLoader);
                return item;
            }
        }
        return null;
    }

    public void changeScreen(ScreenState screenState) {

        this.lightRenderer.extinguish();

        switch (screenState) {
            case TEST:
                this.setScreen(new TestScreen(this, "maps/test.tmx"));
                break;
            case OUTSIDEHOME:
                zone = Zone.OUTSIDE;
                this.setScreen(new OutsidePlayerHomeScreen(this, "maps/outside.tmx"));
                break;
            case HOME:
                zone = Zone.INSIDE;
                this.setScreen(new PlayerHomeScreen(this, "maps/playerHome.tmx"));
                break;
            case CAMP:
                zone = Zone.OUTSIDE;
                this.setScreen(new CampScreen(this, "maps/camping.tmx"));
                break;
            case FARM:
                zone = Zone.OUTSIDE;
                this.setScreen(new FarmScreen(this, "maps/farmland1.tmx"));
                break;
        }
    }

    @Override
    public void dispose() {

    }
}
