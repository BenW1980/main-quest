package apps.peanutpanda.quest;

import apps.peanutpanda.quest.states.ScreenState;
import com.badlogic.gdx.math.Rectangle;


public class ExitMap {

    Rectangle rectangle;
    ScreenState screenState;

    public ExitMap(Rectangle rectangle, ScreenState screenState) {
        this.rectangle = rectangle;
        this.screenState = screenState;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public ScreenState getScreenState() {
        return screenState;
    }
}
