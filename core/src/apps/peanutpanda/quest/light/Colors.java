package apps.peanutpanda.quest.light;

import com.badlogic.gdx.graphics.Color;

public class Colors {

    public Color insideColor;
    public Color preDawnColor;
    public Color dawnColor;
    public Color lightColor;
    public Color preDuskColor;
    public Color duskColor;
    public Color nightColor;

    Colors() {
        preDawnColor = reInitPreDawnColor();
        dawnColor = reInitDawnColor();
        preDuskColor = reInitPreDuskColor();
        duskColor = reInitDuskColor();
        insideColor = reInitInsideColor();
        lightColor = reInitLightColor();
        nightColor = reInitNightColor();

    }

    public Color reInitNightColor() {
        return new Color(0.1f, 0.1f, 0.5f, 1.0f);
    }

    public Color reInitPreDawnColor() {
        return new Color(0.1f, 0.1f, 1.0f, 1.0f);
    }

    public Color reInitDawnColor() {
        return new Color(1.0f, 0.3f, 0.7f, 1.0f);
    }

    public Color reInitLightColor() {
        return new Color(1.0f, 1.0f, 1.0f, 1.0f);
    }

    public Color reInitPreDuskColor() {
        return new Color(1.0f, 0.63f, 0.27f, 1.0f);
    }

    public Color reInitDuskColor() {
        return new Color(1.0f, 0.4f, 0.35f, 1.0f);
    }

    private Color reInitInsideColor() {
        return new Color(0.9f, 0.7f, 0.7f, 1.0f);
    }


}
