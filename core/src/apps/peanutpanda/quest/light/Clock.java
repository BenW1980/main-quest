package apps.peanutpanda.quest.light;

import apps.peanutpanda.quest.assetmanager.AssetLoader;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Clock {

    private float dayTime;
    private Sprite dayCircle;
    private Sprite dayCircleArrow;
    private Sprite clockBack;
    private Sprite hourHandle;
    private Sprite minuteHandle;
    private Stage stage;
    public static final float SPEED_MODIFIER = 60; //60
    private static final float INCREMENT = 1;
    public String time24h = "";
    public String timeAmPm = "";

    public Clock(AssetLoader assetLoader, Stage stage) {
        this.stage = stage;
        this.dayTime = 6;
        this.dayCircle = new Sprite(assetLoader.dayCircle());
        this.dayCircle.setBounds(stage.getWidth() - 65, stage.getHeight() - 60, 45, 50);
        this.dayCircle.setOriginCenter();

        this.clockBack = new Sprite(assetLoader.clockBack());
        this.clockBack.setBounds(stage.getWidth() - 65, stage.getHeight() - 140, 45, 45);

        this.hourHandle = new Sprite(assetLoader.hourHandle());
        this.hourHandle.setBounds(stage.getWidth() - 65, stage.getHeight() - 140, 45, 45);
        this.hourHandle.setOriginCenter();

        this.minuteHandle = new Sprite(assetLoader.minuteHandle());
        this.minuteHandle.setBounds(stage.getWidth() - 65, stage.getHeight() - 140, 45, 45);
        this.minuteHandle.setOriginCenter();

        this.dayCircleArrow = new Sprite(assetLoader.dayCircleArrow());
        this.dayCircleArrow.setBounds(stage.getWidth() - 65, stage.getHeight() - 60, 45, 50);
    }

    public void drawCycle(float delta) {
//        calculateTime();
        advanceTime(delta);
//        calculateRotations();
//        drawTime();
    }

    private void calculateTime() {
        int h = (int) dayTime;
        float m = (((dayTime - h) * 100) / 100) * 60;

        if ((int) m % INCREMENT == 0) {
            calculateTime24H(h, m);
            calculateTimeAmPm(h, m);

        }
    }

    private void calculateTime24H(int h, float m) {

        String hours;
        String minutes;

        hours = (h < 10) ?
                "0" + h :
                "" + h;
        minutes = (m < 10) ?
                "0" + (int) m :
                "" + (int) m;
        this.time24h = hours + ":" + minutes;
    }

    private void calculateTimeAmPm(int h, float m) {

        String hours;
        String minutes;

        if (h > 12) {
            h -= 12;
        }
        hours = (h < 10) ?
                "0" + h :
                "" + h;
        minutes = (m < 10) ?
                "0" + (int) m :
                "" + (int) m;
        this.timeAmPm = (dayTime > 12) ?
                hours + ":" + minutes + " PM" :
                hours + ":" + minutes + " AM";
    }

    private void calculateRotations() {
        dayCircle.setRotation((dayTime * 15) - 180);
        minuteHandle.setRotation(-(dayTime * 360));
        hourHandle.setRotation(minuteHandle.getRotation() / 12);
    }

    private void drawTime() {
        dayCircle.draw(stage.getBatch());
        dayCircleArrow.draw(stage.getBatch());
        clockBack.draw(stage.getBatch());
        minuteHandle.draw(stage.getBatch());
        hourHandle.draw(stage.getBatch());
    }

    private void advanceTime(float delta) {
        this.dayTime += delta / SPEED_MODIFIER;
    }

    public float getDayTime() {
        return dayTime;
    }

    public void setDayTime(float dayTime) {
        this.dayTime = dayTime;
    }
}