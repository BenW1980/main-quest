package apps.peanutpanda.quest.light;

import box2dLight.ConeLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.graphics.Color;

public class LightCone {

    private ConeLight coneLight;
    private boolean rotating;
    public float size;

    LightCone(RayHandler rayHandler, int rays, Color color, float distance,
              float x, float y, float directionDegree, float coneDegree, boolean rotating) {
        this.coneLight = new ConeLight(rayHandler, rays, color, distance, x, y, directionDegree, coneDegree);
        this.size = distance;
        this.rotating = rotating;
    }

    public void rotate(float degree) {
        this.coneLight.setDirection(degree);
    }

    public float getSize() {
        return this.coneLight.getDistance();
    }

    public void setSize(float size) {
        this.coneLight.setDistance(size);
    }

    public boolean isRotating() {
        return rotating;
    }
}
