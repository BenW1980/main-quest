package apps.peanutpanda.quest.light;

import box2dLight.PointLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.graphics.Color;

public class LightPoint {

    private PointLight pointLight;
    private boolean flickering;
    public float size;

    LightPoint(RayHandler rayHandler, int rays, Color color, float distance, float x, float y, boolean flickering) {
        this.pointLight = new PointLight(rayHandler, rays, color, distance, x, y);
        this.size = distance;
        this.flickering = flickering;
    }

    public float getSize() {
        return this.pointLight.getDistance();
    }

    public void setSize(float size) {
        this.pointLight.setDistance(size);
    }

    public boolean isFlickering() {
        return flickering;
    }

}
