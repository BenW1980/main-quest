package apps.peanutpanda.quest.light;

import apps.peanutpanda.quest.states.Zone;
import box2dLight.RayHandler;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;


public class LightRenderer {

    private RayHandler rayHandler;
    private OrthographicCamera camera;

    private Array<LightPoint> lightPoints;
    private Array<LightCone> lightCones;

    private Colors colors;
    private Color preDawnColor;
    private Color dawnColor;
    private Color lightColor;
    private Color preDuskColor;
    private Color duskColor;
    private Color nightColor;

    private World lightWorld;

    public LightRenderer(OrthographicCamera camera) {
        this.colors = new Colors();
        this.initColors();
        this.camera = camera;
        this.lightPoints = new Array<>();
        this.lightCones = new Array<>();
        lightWorld = new World(new Vector2(), true);

        RayHandler.useDiffuseLight(true);
        this.rayHandler = new RayHandler(lightWorld);

    }

    public void extinguish() {
        rayHandler.removeAll();
        this.lightPoints = new Array<>();
        this.lightCones = new Array<>();
    }

    public void createLightPoint(float x, float y, Color color, float size, boolean flicker) {
        lightPoints.add(new LightPoint(rayHandler, 128, color, size, x, y, flicker));

    }

    public void createLightCone(float x, float y, Color color, float size, float directionDegree, float coneDegree, boolean rotating) {
        lightCones.add(new LightCone(rayHandler, 128, color, size, x, y, directionDegree, coneDegree, rotating));

    }

    public void setAmbient(Color color) {
        this.rayHandler.setAmbientLight(color);
    }

    public void rotateLightCones(float rotation, float speed) {
        for (LightCone lightCone : lightCones) {
            if (lightCone.isRotating()) {
                lightCone.rotate(rotation * speed);
            }
        }
    }

    public void render() {

        for (LightPoint lightPoint : lightPoints) {
            if (lightPoint.isFlickering()) {
                lightPoint.setSize(MathUtils.random(lightPoint.size, lightPoint.size + 15));
            }
        }

        this.rayHandler.setCombinedMatrix(this.camera.combined);
        this.rayHandler.updateAndRender();
    }

    private void initColors() {
        this.preDawnColor = colors.preDawnColor;
        this.dawnColor = colors.dawnColor;
        this.lightColor = colors.lightColor;
        this.preDuskColor = colors.preDuskColor;
        this.duskColor = colors.duskColor;
        this.nightColor = colors.nightColor;
    }

    public void setAmbientInside() {
        this.setAmbient(colors.insideColor);
    }

    public void dayNightCycle(Clock clock, Zone zone, float delta) {

        float timeSpeed = Clock.SPEED_MODIFIER / 3.87f;

        if (clock.getDayTime() > 5.50f && clock.getDayTime() <= 6) {
            this.nightColor = colors.reInitNightColor();
            setAmbient(preDawnColor.lerp(dawnColor, delta / (timeSpeed / 2)));
        }
        if (clock.getDayTime() > 6 && clock.getDayTime() <= 18) {
            this.preDawnColor = colors.reInitPreDawnColor();
            setAmbient(dawnColor.lerp(lightColor, delta / timeSpeed));
        }
        if (clock.getDayTime() > 18 && clock.getDayTime() <= 18.50f) {
            this.dawnColor = colors.reInitDawnColor();
            setAmbient(lightColor.lerp(preDuskColor, delta / (timeSpeed / 2)));
        }
        if (clock.getDayTime() > 18.50f && clock.getDayTime() <= 19) {
            this.lightColor = colors.reInitLightColor();
            setAmbient(preDuskColor.lerp(duskColor, delta / (timeSpeed / 2)));
        }
        if (clock.getDayTime() > 19 && clock.getDayTime() <= 24) {
            this.preDuskColor = colors.reInitPreDuskColor();
            setAmbient(duskColor.lerp(nightColor, delta / timeSpeed));
        }
        if (clock.getDayTime() > 0 && clock.getDayTime() <= 5) {
            setAmbient(nightColor);
        }
        if (clock.getDayTime() > 5 && clock.getDayTime() <= 5.50f) {
            this.duskColor = colors.reInitDuskColor();
            setAmbient(nightColor.lerp(preDawnColor, delta / (timeSpeed / 2)));
        }

        if (zone == Zone.INSIDE) {
            setAmbientInside();
        }

        if (clock.getDayTime() > 24) {
            clock.setDayTime(0);
        }
    }

    public void resize(Viewport viewport) {
        this.rayHandler.useCustomViewport(
                viewport.getScreenX(),
                viewport.getScreenY(),
                viewport.getScreenWidth(),
                viewport.getScreenHeight());
    }

    public void dispose() {
        this.lightWorld.dispose();
        this.rayHandler.dispose();

    }
}