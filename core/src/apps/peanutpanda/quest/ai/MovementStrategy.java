package apps.peanutpanda.quest.ai;

import com.badlogic.gdx.scenes.scene2d.Stage;

public interface MovementStrategy {

    void move(Stage stage);
}
