package apps.peanutpanda.quest.ai;

import apps.peanutpanda.quest.gameobjects.NPC;

public abstract class BaseMovement implements MovementStrategy {

    NPC npc;
    float elapsedTime = 0;

    public BaseMovement(NPC npc) {
        this.npc = npc;

    }
}