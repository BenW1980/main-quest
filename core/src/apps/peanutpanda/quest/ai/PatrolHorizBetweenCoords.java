package apps.peanutpanda.quest.ai;

import apps.peanutpanda.quest.gameobjects.NPC;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class PatrolHorizBetweenCoords extends BaseMovement {

    private int x1;
    private int x2;
    private int random;

    public PatrolHorizBetweenCoords(NPC npc, int x1, int x2) {
        super(npc);
        this.x1 = x1;
        this.x2 = x2;
        npc.setWalkingLeftBooleans();
    }

    private int getRandom() {

        if (random == 0) {
            random = MathUtils.random(1, 4);
        }

        return random;
    }

    @Override
    public void move(Stage stage) {

        if (npc.getRect().x < x1) {
            npc.stopAndLookInRandomDirection(getRandom());
            elapsedTime += Gdx.graphics.getDeltaTime();
            if (elapsedTime > 1) {
                npc.setWalkingRightBooleans();
                elapsedTime = 0;
                random = 0;
            }

        } else if (npc.getRect().x > x2) {
            npc.stopAndLookInRandomDirection(getRandom());
            elapsedTime += Gdx.graphics.getDeltaTime();
            if (elapsedTime > 1) {
                npc.setWalkingLeftBooleans();
                elapsedTime = 0;
                random = 0;
            }
        }
    }
}