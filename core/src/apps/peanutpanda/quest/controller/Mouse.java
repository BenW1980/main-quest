package apps.peanutpanda.quest.controller;

import apps.peanutpanda.quest.states.ExitCursorDirection;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.Viewport;

public class Mouse implements InputProcessor {

    private Pixmap pm;
    private Vector3 position;
    private Vector2 mouseLocation;
    private boolean button1Down, button2Down;

    public Mouse() {
        this.mouseLocation = new Vector2(0, 0);
        this.setDefaultCursor();
    }

    public void update(Viewport viewport) {
        position = new Vector3(mouseLocation, 0);
        viewport.getCamera().unproject(position,
                viewport.getScreenX(),
                viewport.getScreenY(),
                viewport.getScreenWidth(),
                viewport.getScreenHeight());
    }

    public void setDefaultCursor() {
        pm = new Pixmap(Gdx.files.internal("images/ui/cursor.png"));
        Gdx.graphics.setCursor(Gdx.graphics.newCursor(pm, 32, 32));
        pm.dispose();
    }

    public void setExitCursor(ExitCursorDirection exitCursorDirection) {

        String path = "";

        switch (exitCursorDirection) {
            case DOWN:
                path = "images/ui/cursorExitDown.png";
                break;
            case UP:
                path = "images/ui/cursorExitUp.png";
                break;
            case LEFT:
                path = "images/ui/cursorExitLeft.png";
                break;
            case RIGHT:
                path = "images/ui/cursorExitRight.png";
                break;

        }

        pm = new Pixmap(Gdx.files.internal(path));
        Gdx.graphics.setCursor(Gdx.graphics.newCursor(pm, 32, 32));
        pm.dispose();
    }

    @Override
    public boolean keyDown(int keycode) {
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (button == 0) {
            button1Down = true;
        } else if (button == 1) {
            button2Down = true;
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (button == 0) {
            button1Down = false;
        } else if (button == 1) {
            button2Down = false;
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        mouseLocation.x = screenX;
        mouseLocation.y = screenY;
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        mouseLocation.x = screenX;
        mouseLocation.y = screenY;
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public Vector3 getPosition() {
        return position;
    }

    public boolean isButton1Down() {
        return button1Down;
    }

    public void setButton1Down(boolean button1Down) {
        this.button1Down = button1Down;
    }

    public boolean isButton2Down() {
        return button2Down;
    }

    public void setButton2Down(boolean button2Down) {
        this.button2Down = button2Down;
    }
}