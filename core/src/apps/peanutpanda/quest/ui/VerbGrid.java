package apps.peanutpanda.quest.ui;

import apps.peanutpanda.quest.states.Verb;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;

public class VerbGrid {

    public VerbButton give;
    public VerbButton pickUp;
    public VerbButton use;
    public VerbButton open;
    public VerbButton talkTo;
    public VerbButton push;
    public VerbButton close;
    public VerbButton lookAt;
    public VerbButton pull;
    public Array<VerbButton> verbBtns;

    public VerbGrid(Command command, BitmapFont fontVerbs) {
        this.verbBtns = new Array<>();

        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.font = fontVerbs;
        textButtonStyle.fontColor = new Color(44 / 255f, 144 / 255f, 295 / 255f, 1);
        textButtonStyle.overFontColor = new Color(149 / 255f, 236 / 255f, 255 / 255f, 1);
        textButtonStyle.checkedFontColor = new Color(149 / 255f, 236 / 255f, 255 / 255f, 1);

        give = new VerbButton(Verb.GIVE, textButtonStyle);
        pickUp = new VerbButton(Verb.PICKUP, textButtonStyle);
        use = new VerbButton(Verb.USE, textButtonStyle);

        open = new VerbButton(Verb.OPEN, textButtonStyle);
        talkTo = new VerbButton(Verb.TALKTO, textButtonStyle);
        push = new VerbButton(Verb.PUSH, textButtonStyle);

        close = new VerbButton(Verb.CLOSE, textButtonStyle);
        lookAt = new VerbButton(Verb.LOOKAT, textButtonStyle);
        pull = new VerbButton(Verb.PULL, textButtonStyle);

        verbBtns.addAll(give, pickUp, use, open, lookAt, push, close, talkTo, pull);

        for (final VerbButton btn : verbBtns) {
            btn.setStyle(textButtonStyle);
            btn.getLabel().setAlignment(Align.left);
            btn.addListener(new ClickListener() {
                public void clicked(InputEvent event, float x, float y) {
                    btn.setChecked(false);
                    command.clear();
                    command.setVerb(btn.getVerb());
                }
            });
        }
    }

    public void addVerbsToTable(Table verbsTable) {
        int width = 60;
        int height = 20;
        int padLeft = 20;
        int padBottom = 2;
        int widLeft = 40;


        verbsTable.clearChildren();

        verbsTable.add(give).size(widLeft, height).padLeft(padLeft - 4).padBottom(padBottom);
        verbsTable.add(pickUp).size(width, height).padLeft(padLeft - 4).padBottom(padBottom);
        verbsTable.add(use).size(widLeft, height).padLeft(padLeft).padBottom(padBottom);
        verbsTable.row();
        verbsTable.add(open).size(widLeft, height).padLeft(padLeft - 4).padBottom(padBottom);
        verbsTable.add(lookAt).size(width, height).padLeft(padLeft).padBottom(padBottom);
        verbsTable.add(push).size(widLeft, height).padLeft(padLeft).padBottom(padBottom);
        verbsTable.row();
        verbsTable.add(close).size(widLeft, height).padLeft(padLeft + 2).padBottom(padBottom);
        verbsTable.add(talkTo).size(width, height).padLeft(padLeft - 4).padBottom(padBottom);
        verbsTable.add(pull).size(widLeft, height).padLeft(padLeft).padBottom(padBottom);

    }
}