package apps.peanutpanda.quest.ui;

import apps.peanutpanda.quest.gameobjects.Entity;
import com.badlogic.gdx.scenes.scene2d.ui.Button;

public class SceneryItemButton extends Button {

    Entity item;

    public SceneryItemButton(Entity item) {
        setStyle(new ButtonStyle());
        this.item = item;
        setSize(item.getRect().width, item.getRect().height);
        setPosition(item.getRect().x, item.getRect().y);
    }


    public Entity getItem() {
        return item;
    }
}
