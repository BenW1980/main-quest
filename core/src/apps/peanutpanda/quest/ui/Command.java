package apps.peanutpanda.quest.ui;

import apps.peanutpanda.quest.gameobjects.Entity;
import apps.peanutpanda.quest.gameobjects.InventoryItem;
import apps.peanutpanda.quest.gameobjects.NPC;
import apps.peanutpanda.quest.states.SingleCommandState;
import apps.peanutpanda.quest.states.Verb;
import com.badlogic.gdx.utils.Array;

public class Command {

    private Array<String> array;
    private Verb verbToHandle;
    private Entity itemToHandle;
    private Entity item2ToHandle;
    private String prepToHandle;
    private boolean waitingToUse;
    public SingleCommandState state;
    private boolean visible;

    public Command() {
        visible = true;
        state = SingleCommandState.NONE;
        this.array = new Array<>();
        this.array.addAll("", "", "", "");
    }

    public void clear() {
        visible = true;
        removeVerb();
        removeItem();
        removePreposition();
        removeItem2();
        state = SingleCommandState.NONE;
        this.array = new Array<>();
        this.array.addAll("", "", "", "");
        waitingToUse = false;
    }

    private boolean verbEqualsUseOrGive() {
        return array.get(0).equals("Use") || array.get(0).equals("Give");
    }

    private String setPrepToHandle() {
        String prep = null;
        if (array.get(0).equals("Use")) {
            prep = "with";
        } else if (array.get(0).equals("Give")) {
            prep = "to";
        }
        return prep;
    }

    public void handle(Entity item) {

        if (!array.get(1).isEmpty()) {
            if (!waitingToUse) {
                if (verbEqualsUseOrGive() && (item instanceof InventoryItem)) {
                    waitingToUse = true;
                    prepToHandle = setPrepToHandle();
                    array.set(2, prepToHandle);
                } else {
                    completeSingleCommand(verbToHandle, itemToHandle);
                }
            } else {
                if (!itemToHandle.getName().toLowerCase().equals(item.getName().toLowerCase())) {
                    item2ToHandle = item;
                    completeUseCommand();
                }
            }
        }
    }

    public void completeSingleCommand(Verb verbToHandle, Entity itemToHandle) {
        if (verbToHandle != null && itemToHandle != null) {

            switch (verbToHandle) {
                case LOOKAT:
                    state = SingleCommandState.LOOKING;
                    break;
                case PICKUP:
                    state = SingleCommandState.TAKING;
                    break;
                case USE:
                    state = SingleCommandState.USING;
                    break;
                case OPEN:
                    state = SingleCommandState.OPENING;
                    break;
                case CLOSE:
                    state = SingleCommandState.CLOSING;
                    break;
                case PUSH:
                    state = SingleCommandState.PUSHING;
                    break;
                case PULL:
                    state = SingleCommandState.PULLING;
                    break;
                case TALKTO:
                    state = SingleCommandState.TALKING;
                    break;
            }
        }
    }

    public void completeUseCommand() {
        if (verbToHandle != null && itemToHandle != null && item2ToHandle != null) {
            if (itemToHandle instanceof InventoryItem) {
                InventoryItem item = (InventoryItem) itemToHandle;

                if (item.canBeUsedWith(item2ToHandle)) {
                    item.setUsedCorrectly(true);
                    state = SingleCommandState.CORRECT_USAGE;
                } else {
                    state = SingleCommandState.WRONG_USAGE;
                }
            }
        }
    }


    public void setVerb(Verb verb) {
        verbToHandle = verb;
        array.set(0, verb.getName());
    }

    public void setItem(Entity item) {
        itemToHandle = item;
        if (array.get(0).isEmpty()) {
            array.set(1, item.getName());
        } else {
            array.set(1, capitalizeOutput(item));
        }
    }

    public void setItem2(Entity item) {
        array.set(3, capitalizeOutput(item));
    }

    private String capitalizeOutput(Entity item) {

        String output;

        if (item instanceof NPC) {
            output = item.getName();
        } else {
            output = item.getName().toLowerCase();
        }

        return output;

    }

    public void removeVerb() {
        verbToHandle = null;
        array.set(0, "");
    }

    public void removeItem() {
        itemToHandle = null;
        array.set(1, "");
    }

    public void removePreposition() {
        prepToHandle = "";
        array.set(2, "");
    }

    public void removeItem2() {
        array.set(3, "");
    }

    public boolean isWaitingToUse() {
        return waitingToUse;
    }

    public String printComplete() {

        String result = "";

        if (visible) {

            if (!array.get(1).toLowerCase().equals(array.get(3).toLowerCase())) {
                result = array.get(0) + " " + array.get(1) + " " + array.get(2) + " " + array.get(3);
            } else {
                result = array.get(0) + " " + array.get(1) + " " + array.get(2);
            }
        }


        return result;
    }


    public Entity getItemToHandle() {
        return itemToHandle;
    }


    public void setVisible(boolean visible) {
        this.visible = visible;
    }

}