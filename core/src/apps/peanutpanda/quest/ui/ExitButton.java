package apps.peanutpanda.quest.ui;

import apps.peanutpanda.quest.states.ExitCursorDirection;
import apps.peanutpanda.quest.states.ScreenState;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.ui.Button;

public class ExitButton extends Button {

    ScreenState state;
    ExitCursorDirection exitCursorDirection;

    public ExitButton(Rectangle rectangle, ScreenState state, ExitCursorDirection exitCursorDirection) {
        this.state = state;
        setStyle(new ButtonStyle());
        setSize(rectangle.getWidth(), rectangle.getHeight());
        setPosition(rectangle.getX(), rectangle.getY());
        this.exitCursorDirection = exitCursorDirection;
    }

    public ExitCursorDirection getExitCursorDirection() {
        return exitCursorDirection;
    }


    public ScreenState getState() {
        return state;
    }
}
