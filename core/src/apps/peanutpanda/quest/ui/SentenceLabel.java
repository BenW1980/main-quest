package apps.peanutpanda.quest.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

public class SentenceLabel extends Label {

    private int id;


    public SentenceLabel(CharSequence text, LabelStyle style, int id) {
        super(text, style);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
