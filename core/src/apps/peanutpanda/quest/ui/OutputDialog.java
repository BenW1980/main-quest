package apps.peanutpanda.quest.ui;

import apps.peanutpanda.quest.assetmanager.AssetLoader;
import apps.peanutpanda.quest.controller.Mouse;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class OutputDialog {

    private float textDrawLength, textSpeed;
    private BitmapFont font;
    private Stage stage;
    private Texture face;
    private Sprite faceBackground;
    private Mouse mouse;
    private Command command;

    public OutputDialog(BitmapFont font, Stage stage, AssetLoader assetLoader, Mouse mouse, Command command) {
        this.font = font;
        this.stage = stage;
        this.mouse = mouse;
        this.command = command;
        this.face = assetLoader.face();
        this.faceBackground = new Sprite(assetLoader.blackBackground());
        faceBackground.setAlpha(0.5f);
        textDrawLength = 0.0f;
        textSpeed = 0.8f;
    }

    public void drawPlayerDialog(String textToPrint) {
        command.setVisible(false);
        faceBackground.setBounds(50, 200, 300, 75);
        faceBackground.draw(stage.getBatch());
        stage.getBatch().draw(face, 55, 203, 50, 70);

        if ((int) textDrawLength < textToPrint.length()) {
            textDrawLength += textSpeed;
        }
        font.draw(stage.getBatch(), (textToPrint.substring(0, (int) textDrawLength)), 110, 265);

        if (mouse.isButton1Down() || mouse.isButton2Down()) {
            reset();
        }
    }

    public void drawNpcDialog(String textToPrint, Sprite npcCloseUp) {
        command.setVisible(false);
        faceBackground.setBounds(50, 200, 300, 75);
        faceBackground.draw(stage.getBatch());
        stage.getBatch().draw(npcCloseUp, 295, 203, 50, 70);

        if ((int) textDrawLength < textToPrint.length()) {
            textDrawLength += textSpeed;
        }
        font.draw(stage.getBatch(), (textToPrint.substring(0, (int) textDrawLength)), 55, 265);

        if (mouse.isButton1Down() || mouse.isButton2Down()) {
            reset();
        }
    }

    private void reset() {
        command.clear();
        textDrawLength = 0.0f;
    }

}