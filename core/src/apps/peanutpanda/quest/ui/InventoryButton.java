package apps.peanutpanda.quest.ui;

import apps.peanutpanda.quest.gameobjects.InventoryItem;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.ui.Button;

public class InventoryButton extends Button {

    InventoryItem item;

    public InventoryButton(Sprite bounds) {
        setStyle(new ButtonStyle());
        setSize(bounds.getWidth(), bounds.getHeight());
        setPosition(bounds.getX(), bounds.getY());
    }


    public InventoryItem getItem() {
        return item;
    }
}
