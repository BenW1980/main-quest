package apps.peanutpanda.quest.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class ConversationGrid {

    public Stage stage;
    private SentenceLabel sentence1;
    private SentenceLabel sentence2;
    private SentenceLabel sentence3;
    private SentenceLabel sentence4;

    private Array<SentenceLabel> sentenceLabels;

    private int sentenceChosen;

    public ConversationGrid(BitmapFont fontOutputDialog, Stage stage) {
        this.stage = stage;
        this.sentenceLabels = new Array<>();

        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.font = fontOutputDialog;

        sentence1 = new SentenceLabel("", labelStyle, 1);
        sentence2 = new SentenceLabel("", labelStyle, 2);
        sentence3 = new SentenceLabel("", labelStyle, 3);
        sentence4 = new SentenceLabel("", labelStyle, 4);

        sentenceLabels.addAll(sentence1, sentence2, sentence3, sentence4);

        for (SentenceLabel label : sentenceLabels) {
            label.setAlignment(Align.left);
            label.addListener(
                    new ClickListener() {
                        @Override
                        public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                            label.setColor(Color.SALMON);
                        }

                        @Override
                        public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                            label.setColor(labelStyle.font.getColor());
                        }

                        public void clicked(InputEvent event, float x, float y) {
                            sentenceChosen = label.getId();

                        }
                    }
            );
        }

    }

    public void hideSentences() {
        for (SentenceLabel sentenceLabel : sentenceLabels) {
            sentenceLabel.clearActions();
            sentenceLabel.setVisible(false);

        }
    }

    public void revealSentences() {
        for (SentenceLabel sentenceLabel : sentenceLabels) {
            sentenceLabel.setVisible(true);
            sentenceLabel.getColor().a = 0;
        }

//        sentence1.addAction(Actions.moveTo((float) Math.random() * 200, (float) Math.random() * 200, .5f, Interpolation.pow2Out));
//        sentence2.addAction(Actions.moveTo((float) Math.random() * 200, (float) Math.random() * 200, .5f, Interpolation.pow2Out));
//        sentence3.addAction(Actions.moveTo((float) Math.random() * 200, (float) Math.random() * 200, .5f, Interpolation.pow2Out));
//        sentence4.addAction(Actions.moveTo((float) Math.random() * 200, (float) Math.random() * 200, .5f, Interpolation.pow2Out));


        sentence1.addAction(sequence(moveTo(5, 50, 0.10f, Interpolation.pow2Out), fadeIn(0.5f)));
        sentence2.addAction(sequence(moveTo(5, 35, 0.12f, Interpolation.pow2Out), fadeIn(0.5f)));
        sentence3.addAction(sequence(moveTo(5, 20, 0.14f, Interpolation.pow2Out), fadeIn(0.5f)));
        sentence4.addAction(sequence(moveTo(5, 5, 0.16f, Interpolation.pow2Out), fadeIn(0.5f)));

    }

    public void addSentencesToTable(Table verbsTable, Array<String> conversation) {

        int width = (int) stage.getWidth();
        int height = 15;
        int padLeft = 10;

        for (int i = 0; i < sentenceLabels.size; i++) {
            sentenceLabels.get(i).setText("* " + conversation.get(i));
            sentenceLabels.get(i).clearActions();
        }

        verbsTable.clearChildren();

        revealSentences();

        verbsTable.add(sentence1).size(width, height).padLeft(padLeft);
        verbsTable.row();
        verbsTable.add(sentence2).size(width, height).padLeft(padLeft);
        verbsTable.row();
        verbsTable.add(sentence3).size(width, height).padLeft(padLeft);
        verbsTable.row();
        verbsTable.add(sentence4).size(width, height).padLeft(padLeft).padBottom(5);
        verbsTable.row();

    }

    public void setSentenceChosen(int sentenceChosen) {
        this.sentenceChosen = sentenceChosen;
    }

    public int getSentenceChosen() {
        return sentenceChosen;
    }

}
