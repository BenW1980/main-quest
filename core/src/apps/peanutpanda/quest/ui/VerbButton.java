package apps.peanutpanda.quest.ui;

import apps.peanutpanda.quest.states.Verb;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class VerbButton extends TextButton {

    private Verb verb;

    public VerbButton(Verb verb, TextButtonStyle textButtonStyle) {
        super(verb.getName(), textButtonStyle);
        this.verb = verb;
    }

    public Verb getVerb() {
        return verb;
    }
}