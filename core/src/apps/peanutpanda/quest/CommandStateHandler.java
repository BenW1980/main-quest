package apps.peanutpanda.quest;

import apps.peanutpanda.quest.gameobjects.MapInventoryItem;
import apps.peanutpanda.quest.gameobjects.NPC;
import apps.peanutpanda.quest.ui.Command;
import apps.peanutpanda.quest.ui.OutputDialog;

public class CommandStateHandler {

    private Command command;
    private boolean conversationEntered;
    private NPC talkingNpc;

    public CommandStateHandler(Command command) {
        this.command = command;
    }

    public void checkCommandState(OutputDialog outputDialog) {

        switch (command.state) {

            case LOOKING:
                if (command.getItemToHandle() != null) {
                    outputDialog.drawPlayerDialog(command.getItemToHandle().getDescr());
                } else {
                    command.clear();
                }
                break;

            case TAKING:
                String pickUpTxt = "";

                if (command.getItemToHandle() != null) {
                    if (command.getItemToHandle() instanceof MapInventoryItem) {
                        if (((MapInventoryItem) command.getItemToHandle()).isFreeToTake()) {
                            pickUpTxt = command.getItemToHandle().getPickUpDescr();
                            ((MapInventoryItem) command.getItemToHandle()).setTaken(true);
                        } else {
                            pickUpTxt = ((MapInventoryItem) command.getItemToHandle()).getPickUpNotFreeDescr();
                        }

                    } else {
                        pickUpTxt = command.getItemToHandle().getPickUpDescr();
                    }
                } else {
                    command.clear();
                }

                outputDialog.drawPlayerDialog(pickUpTxt);
                break;

            case USING:
                outputDialog.drawPlayerDialog(command.getItemToHandle().getUseDescr());
                break;
            case OPENING:
                outputDialog.drawPlayerDialog(command.getItemToHandle().getOpenDescr());
                break;
            case CLOSING:
                outputDialog.drawPlayerDialog(command.getItemToHandle().getCloseDescr());
                break;
            case PUSHING:
                outputDialog.drawPlayerDialog(command.getItemToHandle().getPushDescr());
                break;
            case PULLING:
                outputDialog.drawPlayerDialog(command.getItemToHandle().getPullDescr());
                break;
            case TALKING:
                if (command.getItemToHandle() != null) {
                    if ((command.getItemToHandle() instanceof NPC)) {
                        conversationEntered = true;
                        talkingNpc = (NPC) command.getItemToHandle();

                    } else {
                        outputDialog.drawPlayerDialog(command.getItemToHandle().getTalkDescr());
                    }
                } else {
                    command.clear();
                }
                break;

            case WRONG_USAGE:
                outputDialog.drawPlayerDialog(command.getItemToHandle().getResponseOnWrongUsage());
                break;

            case CORRECT_USAGE:
                outputDialog.drawPlayerDialog(command.getItemToHandle().getResponseOnCorrectUsage());
                break;

        }
    }

    public boolean isConversationEntered() {
        return conversationEntered;
    }

    public void setConversationEntered(boolean conversationEntered) {
        this.conversationEntered = conversationEntered;
    }

    public NPC getTalkingNpc() {
        return talkingNpc;
    }

}
