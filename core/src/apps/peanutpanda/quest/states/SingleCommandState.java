package apps.peanutpanda.quest.states;

public enum SingleCommandState {

    NONE, LOOKING, TAKING, USING, OPENING, CLOSING, PUSHING, PULLING, TALKING, WRONG_USAGE, CORRECT_USAGE;
}
