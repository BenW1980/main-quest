package apps.peanutpanda.quest.states;

public enum ScreenSize {

    WIDTH(512), HEIGHT(288); // TILED : 32/18

    private final int size;

    ScreenSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

}