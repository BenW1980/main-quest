package apps.peanutpanda.quest.states;

public enum GameState {

    IN_GAME, IN_CONVERSATION;

}
