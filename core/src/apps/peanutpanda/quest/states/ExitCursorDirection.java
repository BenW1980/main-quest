package apps.peanutpanda.quest.states;

public enum ExitCursorDirection {

    LEFT, RIGHT, UP, DOWN
}
