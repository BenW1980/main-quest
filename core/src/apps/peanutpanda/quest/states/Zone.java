package apps.peanutpanda.quest.states;

public enum Zone {

    INSIDE, OUTSIDE
}
