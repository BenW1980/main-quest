package apps.peanutpanda.quest.states;

public enum ScreenState {

    OUTSIDEHOME, HOME, CAMP, FARM, TEST;

}
