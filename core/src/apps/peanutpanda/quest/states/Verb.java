package apps.peanutpanda.quest.states;

public enum Verb {

    GIVE("Give"), PICKUP("Pick up"), USE("Use"), OPEN("Open"), LOOKAT("Look at"), PUSH("Push"), CLOSE("Close"), TALKTO("Talk to"), PULL("Pull");

    private final String name;

    Verb(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
