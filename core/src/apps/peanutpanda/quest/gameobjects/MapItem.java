package apps.peanutpanda.quest.gameobjects;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;

public class MapItem extends Entity {

    MapItem() {

    }

    public void create(TiledMap tiledMap) {
        MapObject mapObject = tiledMap.getLayers().get(id).getObjects().get(0);
        rect = ((RectangleMapObject) mapObject).getRectangle();
        centerX = rect.x + rect.width / 2;
        centerY = rect.y + rect.height / 2;
    }
}
