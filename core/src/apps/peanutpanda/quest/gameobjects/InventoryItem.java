package apps.peanutpanda.quest.gameobjects;

import apps.peanutpanda.quest.states.Verb;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;


public class InventoryItem extends Entity {

    private Sprite sprite;
    private Entity useWithItem;
    private boolean usedCorrectly;

    public InventoryItem(String name, Texture texture) {
        this.name = name;
        this.rect = new Rectangle();
        this.sprite = new Sprite(texture);
        this.responseOnWrongUsage = "That doesn't work.";
        this.defaultVerb = Verb.LOOKAT.getName();
    }

    public void setBounds(Sprite bounds) {
        this.sprite.setBounds(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
        this.rect.set(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
        centerX = rect.x + rect.width / 2;
        centerY = rect.y + rect.height / 2;
    }

    public boolean canBeUsedWith(Entity entity) {
        return entity == useWithItem;
    }

    public Entity getUseWithItem() {
        return useWithItem;
    }

    public void setUseWithItem(Entity useWithItem) {
        this.useWithItem = useWithItem;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }

    public boolean isUsedCorrectly() {
        return usedCorrectly;
    }

    public void setUsedCorrectly(boolean usedCorrectly) {
        this.usedCorrectly = usedCorrectly;
    }

}
