package apps.peanutpanda.quest.gameobjects;

import apps.peanutpanda.quest.assetmanager.AssetLoader;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;

public class MapInventoryItem extends MapItem {

    private boolean taken;
    private boolean freeToTake;
    private String pickUpNotFreeDescr;
    private int width;
    private int height;
    private String textureLoc;
    private Texture texture;

    public MapInventoryItem() {

    }

    public void create(AssetLoader assetLoader, TiledMap tiledMap) {
        freeToTake = true;
        texture = assetLoader.manager.get(textureLoc, Texture.class);
        MapObject mapObject = tiledMap.getLayers().get(id).getObjects().get(0);
        rect = ((RectangleMapObject) mapObject).getRectangle();
        centerX = rect.x + rect.width / 2;
        centerY = rect.y + rect.height / 2;
    }

    public boolean isTaken() {
        return taken;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }

    public boolean isFreeToTake() {
        return freeToTake;
    }

    public void setFreeToTake(boolean freeToTake) {
        this.freeToTake = freeToTake;
    }

    public String getPickUpNotFreeDescr() {
        return pickUpNotFreeDescr;
    }

    public void setPickUpNotFreeDescr(String pickUpNotFreeDescr) {
        this.pickUpNotFreeDescr = pickUpNotFreeDescr;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Texture getTexture() {
        return texture;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }
}
