package apps.peanutpanda.quest.gameobjects;

import apps.peanutpanda.quest.states.Verb;
import apps.peanutpanda.quest.ui.VerbButton;
import apps.peanutpanda.quest.ui.VerbGrid;
import com.badlogic.gdx.math.Rectangle;

public abstract class Entity {

    protected String id;
    protected String name;
    protected Rectangle rect;
    public float centerX;
    public float centerY;
    protected String defaultVerb;

    protected String descr;
    protected String pickUpDescr;
    protected String useDescr;
    protected String pushDescr;
    protected String pullDescr;
    protected String openDescr;
    protected String closeDescr;
    protected String talkDescr;

    protected String responseOnWrongUsage;
    protected String responseOnCorrectUsage;


    public Entity() {
        descr = "Nice!";
        pickUpDescr = "I can't pick that up.";
        useDescr = "That's not something I can use.";
        pushDescr = "I can't push that.";
        pullDescr = "I can't pull that.";
        openDescr = "I can't open that.";
        closeDescr = "I can't close that.";
        talkDescr = "Hellooooo?!";

    }

    public Verb findDefaultVerbByName() {

        Verb verb = null;

        if (defaultVerb.equals(Verb.LOOKAT.getName())) {
            verb = Verb.LOOKAT;
        } else if (defaultVerb.equals(Verb.OPEN.getName())) {
            verb = Verb.OPEN;
        } else if (defaultVerb.equals(Verb.CLOSE.getName())) {
            verb = Verb.CLOSE;
        } else if (defaultVerb.equals(Verb.TALKTO.getName())) {
            verb = Verb.TALKTO;
        }

        return verb;
    }

    public VerbButton findDefaultVerbButton(VerbGrid verbGrid) {

        VerbButton verbButton = null;

        switch (findDefaultVerbByName()) {
            case LOOKAT:
                verbButton = verbGrid.lookAt;
                break;
            case OPEN:
                verbButton = verbGrid.open;
                break;
            case CLOSE:
                verbButton = verbGrid.close;
                break;
            case TALKTO:
                verbButton = verbGrid.talkTo;
                break;
        }

        return verbButton;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Rectangle getRect() {
        return rect;
    }

    public void setRect(Rectangle rect) {
        this.rect = rect;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getPickUpDescr() {
        return pickUpDescr;
    }

    public void setPickUpDescr(String pickUpDescr) {
        this.pickUpDescr = pickUpDescr;
    }

    public String getUseDescr() {
        return useDescr;
    }

    public void setUseDescr(String useDescr) {
        this.useDescr = useDescr;
    }

    public String getPushDescr() {
        return pushDescr;
    }

    public void setPushDescr(String pushDescr) {
        this.pushDescr = pushDescr;
    }

    public String getPullDescr() {
        return pullDescr;
    }

    public void setPullDescr(String pullDescr) {
        this.pullDescr = pullDescr;
    }

    public String getOpenDescr() {
        return openDescr;
    }

    public void setOpenDescr(String openDescr) {
        this.openDescr = openDescr;
    }

    public String getCloseDescr() {
        return closeDescr;
    }

    public void setCloseDescr(String closeDescr) {
        this.closeDescr = closeDescr;
    }

    public String getTalkDescr() {
        return talkDescr;
    }

    public void setTalkDescr(String talkDescr) {
        this.talkDescr = talkDescr;
    }

    public String getDefaultVerb() {
        return defaultVerb;
    }

    public void setDefaultVerb(String defaultVerb) {
        this.defaultVerb = defaultVerb;
    }

    public String getResponseOnWrongUsage() {
        return responseOnWrongUsage;
    }

    public String getResponseOnCorrectUsage() {
        return responseOnCorrectUsage;
    }

    public void setResponseOnWrongUsage(String responseOnWrongUsage) {
        this.responseOnWrongUsage = responseOnWrongUsage;
    }

    public void setResponseOnCorrectUsage(String responseOnCorrectUsage) {
        this.responseOnCorrectUsage = responseOnCorrectUsage;
    }
}
