package apps.peanutpanda.quest.gameobjects;

import apps.peanutpanda.quest.ai.MovementStrategy;
import apps.peanutpanda.quest.assetmanager.AssetLoader;
import apps.peanutpanda.quest.states.GameState;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;

public class NPC extends Character {

    private String walkAtlasLoc;
    private String closeUpLoc;
    private Sprite closeUpSprite;

    private Array<String> conversations;
    private Array<String> responses;

    private MovementStrategy movementStrategy;

    private TextureAtlas.AtlasRegion standRegion;

    public NPC() {

    }

    public void create(AssetLoader assLoader, float x, float y) {

        width = 12;
        height = 22;

        animationSpeed = 4.0f;
        walkingSpeed = 20.0f;

        walkAtlas = assLoader.manager.get(walkAtlasLoc, TextureAtlas.class);
        closeUpSprite = new Sprite(assLoader.manager.get(closeUpLoc, Texture.class));

        this.rect = new Rectangle(x - width, y - width, width * 3, height + width); //  rect voor hover en interacties - extra groot gemaakt
        centerX = rect.x + rect.width / 2;
        centerY = rect.y + rect.height / 2;

        createAnimation();

    }


    public void stopAndLookInRandomDirection(int loc) {
        standRegion = standDown;

        switch (loc) {
            case 1:
                standRegion = standDown;
                break;
            case 2:
                standRegion = standLeft;
                break;
            case 3:
                standRegion = standRight;
                break;
            case 4:
                standRegion = standUp;
                break;
        }

        setNotWalkingBooleans();
    }

    public void move(float delta, Stage stage, GameState gameState) {

        charSprite.setBounds(this.rect.x, this.rect.y, width, height);
        charSprite.draw(stage.getBatch());

        if (gameState == GameState.IN_GAME) {

            if (walkingLeft) {
                walkLeft(delta, stage);
            } else if (walkingRight) {
                walkRight(delta, stage);
            } else if (walkingUp) {
                walkUp(delta, stage);
            } else if (walkingDown) {
                walkDown(delta, stage);
            } else {
                stopWalking(stage, standRegion);
            }

            movementStrategy.move(stage);

        } else if (gameState == GameState.IN_CONVERSATION) {
            stopWalking(stage, standDown);
        }

    }

    public void setMovementStrategy(MovementStrategy movementStrategy) {
        this.movementStrategy = movementStrategy;
    }

    public Array<String> getConversations() {
        return conversations;
    }

    public Array<String> getResponses() {
        return responses;
    }

    public Sprite getCloseUpSprite() {
        return closeUpSprite;
    }
}
