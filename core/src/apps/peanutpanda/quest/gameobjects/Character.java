package apps.peanutpanda.quest.gameobjects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;

public class Character extends Entity {

    Sprite charSprite;

    TextureAtlas walkAtlas;

    Animation<TextureRegion> walkLeftAnim;
    Animation<TextureRegion> walkRightAnim;
    Animation<TextureRegion> walkDownAnim;
    Animation<TextureRegion> walkUpAnim;

    TextureAtlas.AtlasRegion standLeft;
    TextureAtlas.AtlasRegion standRight;
    TextureAtlas.AtlasRegion standDown;
    TextureAtlas.AtlasRegion standUp;

    boolean walkingLeft, walkingRight, walkingUp, walkingDown, notWalking;

    int width, height;
    float walkingSpeed;
    private float elapsedTime = 0f;
    float animationSpeed;
    float maxIdleTime = 50;

    Character() {
    }

    void createAnimation() {

        Array<TextureAtlas.AtlasRegion> walkLeftRegions = new Array<>();
        Array<TextureAtlas.AtlasRegion> walkRightRegions = new Array<>();
        Array<TextureAtlas.AtlasRegion> walkUpRegions = new Array<>();
        Array<TextureAtlas.AtlasRegion> walkDownRegions = new Array<>();

        walkLeftRegions.add(walkAtlas.findRegion("left_walk1"));
        walkLeftRegions.add(walkAtlas.findRegion("left_walk2"));

        walkRightRegions.add(walkAtlas.findRegion("right_walk1"));
        walkRightRegions.add(walkAtlas.findRegion("right_walk2"));

        walkUpRegions.add(walkAtlas.findRegion("up_walk1"));
        walkUpRegions.add(walkAtlas.findRegion("up_walk2"));

        walkDownRegions.add(walkAtlas.findRegion("down_walk1"));
        walkDownRegions.add(walkAtlas.findRegion("down_walk2"));

        walkLeftAnim = new Animation<>(1f / animationSpeed, walkLeftRegions);
        walkRightAnim = new Animation<>(1f / animationSpeed, walkRightRegions);
        walkDownAnim = new Animation<>(1f / animationSpeed, walkDownRegions);
        walkUpAnim = new Animation<>(1f / animationSpeed, walkUpRegions);

        standLeft = walkAtlas.findRegion("left_stand");
        standRight = walkAtlas.findRegion("right_stand");
        standDown = walkAtlas.findRegion("down_stand");
        standUp = walkAtlas.findRegion("up_stand");

        if (charSprite == null) {
            charSprite = new Sprite(walkAtlas.findRegion("down_stand"));
        } else {
            setLookDirection();
        }


    }

    private void setLookDirection() {

        if (walkingLeft) {
            charSprite = new Sprite(walkAtlas.findRegion("left_stand"));
        } else if (walkingRight) {
            charSprite = new Sprite(walkAtlas.findRegion("right_stand"));
        } else if (walkingUp) {
            charSprite = new Sprite(walkAtlas.findRegion("up_stand"));
        } else if (walkingDown) {
            charSprite = new Sprite(walkAtlas.findRegion("down_stand"));
        }
    }


    //links/rechts
    void walkLeft(float delta, Stage stage) {
        setWalkingLeftBooleans();
        elapsedTime += delta;
        charSprite.setRegion(walkAtlas.findRegion("left_stand"));
        rect.x -= delta * walkingSpeed;
        stage.getBatch().draw(walkLeftAnim.getKeyFrame(elapsedTime, true), rect.x, rect.y, width, height);
    }

    void walkRight(float delta, Stage stage) {
        setWalkingRightBooleans();
        elapsedTime += delta;
        charSprite.setRegion(walkAtlas.findRegion("right_stand"));
        rect.x += delta * walkingSpeed;
        stage.getBatch().draw(walkRightAnim.getKeyFrame(elapsedTime, true), rect.x, rect.y, width, height);
    }

    void walkUp(float delta, Stage stage) {
        setWalkingUpBooleans();
        elapsedTime += delta;
        charSprite.setRegion(walkAtlas.findRegion("up_stand"));
        rect.y += delta * walkingSpeed;
        stage.getBatch().draw(walkUpAnim.getKeyFrame(elapsedTime, true), rect.x, rect.y, width, height);
    }

    void walkDown(float delta, Stage stage) {
        setWalkingDownBooleans();
        elapsedTime += delta;
        charSprite.setRegion(walkAtlas.findRegion("down_stand"));
        rect.y -= delta * walkingSpeed;
        stage.getBatch().draw(walkDownAnim.getKeyFrame(elapsedTime, true), rect.x, rect.y, width, height);
    }

    public void stopWalking(Stage stage, TextureAtlas.AtlasRegion region) {
        charSprite.setRegion(region);
        stage.getBatch().draw(region, rect.x, rect.y, width, height);
    }

    void stand(Stage stage, float idleTime) {
        if (idleTime <= maxIdleTime) {
            stage.getBatch().draw(charSprite, rect.x, rect.y, width, height);
        }
    }

    public void setWalkingLeftBooleans() {
        walkingLeft = true;
        walkingRight = false;
        walkingUp = false;
        walkingDown = false;
        notWalking = false;
    }

    public void setWalkingRightBooleans() {
        walkingLeft = false;
        walkingRight = true;
        walkingUp = false;
        walkingDown = false;
        notWalking = false;
    }

    public void setWalkingDownBooleans() {
        walkingLeft = false;
        walkingRight = false;
        walkingUp = false;
        walkingDown = true;
        notWalking = false;
    }

    public void setWalkingUpBooleans() {
        walkingLeft = false;
        walkingRight = false;
        walkingUp = true;
        walkingDown = false;
        notWalking = false;
    }

    public void setNotWalkingBooleans() {
        walkingLeft = false;
        walkingRight = false;
        walkingUp = false;
        walkingDown = false;
        notWalking = true;
    }

    public boolean isWalkingLeft() {
        return walkingLeft;
    }

    public boolean isWalkingRight() {
        return walkingRight;
    }

    public boolean isWalkingUp() {
        return walkingUp;
    }

    public boolean isWalkingDown() {
        return walkingDown;
    }

    public Sprite getCharSprite() {
        return charSprite;
    }

    public void setCharSprite(Sprite charSprite) {
        this.charSprite = charSprite;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public TextureAtlas.AtlasRegion getStandLeft() {
        return standLeft;
    }

    public TextureAtlas.AtlasRegion getStandRight() {
        return standRight;
    }

    public TextureAtlas.AtlasRegion getStandDown() {
        return standDown;
    }

    public TextureAtlas.AtlasRegion getStandUp() {
        return standUp;
    }

    public TextureAtlas getWalkAtlas() {
        return walkAtlas;
    }

    public void setWalkAtlas(TextureAtlas walkAtlas) {
        this.walkAtlas = walkAtlas;
    }
}
