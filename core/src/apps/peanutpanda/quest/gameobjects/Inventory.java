package apps.peanutpanda.quest.gameobjects;

import apps.peanutpanda.quest.ui.Command;
import apps.peanutpanda.quest.ui.InventoryButton;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;


public class Inventory {

    private Array<InventoryItem> items;
    private Array<Sprite> invBtnBackground;
    private Array<InventoryButton> inventoryButtons;
    private Stage stage;
    private Command command;

    public Inventory(Array<Sprite> invBtnBackground, Stage stage, Command command) {
        items = new Array<>();
        this.invBtnBackground = invBtnBackground;
        this.inventoryButtons = new Array<>();
        this.stage = stage;
        this.command = command;
    }

    public void add(InventoryItem inventoryItem) {

        if (!items.contains(inventoryItem, true)) {

            this.items.add(inventoryItem);
            inventoryItem.setBounds(invBtnBackground.get(items.size - 1));
            inventoryButtons.add(new InventoryButton(invBtnBackground.get(items.size - 1)));

            for (int i = 0; i < inventoryButtons.size; i++) {
                int finalI = i;
                inventoryButtons.get(i).addListener(new ClickListener() {
                    public void clicked(InputEvent event, float x, float y) {
                        command.handle(items.get(finalI));
                    }
                });
                inventoryButtons.get(i).addListener(new ClickListener(Input.Buttons.RIGHT) {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        command.completeSingleCommand(inventoryItem.findDefaultVerbByName(), items.get(finalI));

                    }
                });
                stage.addActor(inventoryButtons.get(i));
            }
        }
    }

    public Array<InventoryItem> getItems() {
        return items;
    }
}
