package apps.peanutpanda.quest.gameobjects;

import apps.peanutpanda.quest.assetmanager.AssetLoader;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class AnimatedItem extends MapItem {

    private Animation<TextureRegion> animation;
    private String atlasPath;

    public AnimatedItem() {

    }

    public void create(TiledMap tiledMap, AssetLoader assloader) {
        super.create(tiledMap);
        TextureAtlas atlas = assloader.manager.get(atlasPath, TextureAtlas.class);
        animation = new Animation<>(1f / 15f, atlas.getRegions());
        centerX = rect.x + rect.width / 2;
        centerY = rect.y + rect.height / 2;
    }

    public void animate(Stage stage, float elapsedTime) {
        stage.getBatch().draw(animation.getKeyFrame(elapsedTime / 2f, true), rect.x, rect.y, rect.width, rect.height);
    }

}
