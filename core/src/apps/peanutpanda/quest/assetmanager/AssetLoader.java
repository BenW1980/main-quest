package apps.peanutpanda.quest.assetmanager;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class AssetLoader {

    public final AssetManager manager = new AssetManager();

    public void loadFontHandling() {

        FileHandleResolver resolver = new InternalFileHandleResolver();
        manager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
        manager.setLoader(BitmapFont.class, ".ttf", new FreetypeFontLoader(resolver));

        FreetypeFontLoader.FreeTypeFontLoaderParameter monkeySmall = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        monkeySmall.fontFileName = "fonts/monkey.ttf";
        monkeySmall.fontParameters.size = 33;
        monkeySmall.fontParameters.color = Color.ORANGE;
        monkeySmall.fontParameters.borderWidth = 5;
        monkeySmall.fontParameters.borderColor = Color.BLACK;
        manager.load("monkey20.ttf", BitmapFont.class, monkeySmall);

        FreetypeFontLoader.FreeTypeFontLoaderParameter monkey35 = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        monkey35.fontFileName = "fonts/monkey.ttf";
        monkey35.fontParameters.size = 35;
        monkey35.fontParameters.borderWidth = 5;
        monkey35.fontParameters.borderColor = Color.BLACK;
        manager.load("monkey35.ttf", BitmapFont.class, monkey35);

        FreetypeFontLoader.FreeTypeFontLoaderParameter monkey50 = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        monkey50.fontFileName = "fonts/monkey.ttf";
        monkey50.fontParameters.size = 50;
        monkey50.fontParameters.borderWidth = 5;
        monkey50.fontParameters.borderColor = Color.BLACK;
        monkey50.fontParameters.shadowOffsetY = 3;
        monkey50.fontParameters.shadowColor = Color.BLACK;
        manager.load("monkey50.ttf", BitmapFont.class, monkey50);
    }

    public void loadTiledMaps() {
        manager.setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));
        manager.load("maps/outside.tmx", TiledMap.class);
        manager.load("maps/playerHome.tmx", TiledMap.class);
        manager.load("maps/camping.tmx", TiledMap.class);
        manager.load("maps/farmland1.tmx", TiledMap.class);
    }

    public void loadAtlas() {
        manager.load("animations/fire.atlas", TextureAtlas.class);
        manager.load("animations/charAnim/npc/farmer/farmer.atlas", TextureAtlas.class);
    }

    public void loadImages() {
        manager.load("images/ui/blackBackground.png", Texture.class);
        manager.load("images/ui/invBtnBack.png", Texture.class);

        manager.load("images/ui/dayCircle.png", Texture.class);
        manager.load("images/ui/dayCircleArrow.png", Texture.class);
        manager.load("images/ui/clockBack.png", Texture.class);
        manager.load("images/ui/hourHandle.png", Texture.class);
        manager.load("images/ui/minuteHandle.png", Texture.class);

        manager.load("images/inventory/book.png", Texture.class);
        manager.load("images/inventory/fire.png", Texture.class);
        manager.load("images/inventory/tomato.png", Texture.class);
        manager.load("images/face.png", Texture.class);

    }

    public void loadMusic() {
        manager.load("music/sv.ogg", Music.class);
    }

    public Music starDew() {
        return manager.get("music/sv.ogg", Music.class);
    }

    public Texture tomato() {
        return manager.get("images/inventory/tomato.png", Texture.class);
    }

    public Texture blackCircle() {
        return manager.get("images/ui/blackCircle.png", Texture.class);
    }

    public Texture dayCircle() {
        return manager.get("images/ui/dayCircle.png", Texture.class);
    }

    public Texture clockBack() {
        return manager.get("images/ui/clockBack.png", Texture.class);
    }

    public Texture minuteHandle() {
        return manager.get("images/ui/minuteHandle.png", Texture.class);
    }

    public Texture hourHandle() {
        return manager.get("images/ui/hourHandle.png", Texture.class);
    }

    public Texture dayCircleArrow() {
        return manager.get("images/ui/dayCircleArrow.png", Texture.class);
    }

    public Texture face() {
        return manager.get("images/face.png", Texture.class);
    }

    public Texture invBtnBack() {
        return manager.get("images/ui/invBtnBack.png", Texture.class);
    }

    public Texture book() {
        return manager.get("images/inventory/book.png", Texture.class);
    }

    public Texture fire() {
        return manager.get("images/inventory/fire.png", Texture.class);
    }

    public Texture blackBackground() {
        return manager.get("images/ui/blackBackground.png", Texture.class);
    }

    public void loadSkins() {
        SkinLoader.SkinParameter params = new SkinLoader.SkinParameter("skin/skin.atlas");
        manager.load("skin/skin.json", Skin.class, params);
    }

    public Skin skin() {
        return manager.get("skin/skin.json", Skin.class);
    }


}
