package apps.peanutpanda.quest.maphandling;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;

public class MapBleedFixer {

    public static void fixTiledMapBleed(TiledMap tiledMap) {
        for (TiledMapTileSet tileSet : tiledMap.getTileSets()) {
            for (TiledMapTile tiledMapTile : tileSet) {
                fixBleeding(tiledMapTile.getTextureRegion());
            }
        }
    }

    private static void fixBleeding(TextureRegion region) {
        float fix = 0.01f;
        float x = region.getRegionX();
        float y = region.getRegionY();
        float width = region.getRegionWidth();
        float height = region.getRegionHeight();
        float invTexWidth = 1f / region.getTexture().getWidth();
        float invTexHeight = 1f / region.getTexture().getHeight();
        region.setRegion((x + fix) * invTexWidth,
                (y + fix) * invTexHeight,
                (x + width - fix) * invTexWidth,
                (y + height - fix) * invTexHeight);
    }
}
