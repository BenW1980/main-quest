package apps.peanutpanda.quest.screens;

import apps.peanutpanda.quest.Quest;
import apps.peanutpanda.quest.gameobjects.AnimatedItem;
import apps.peanutpanda.quest.gameobjects.MapInventoryItem;
import apps.peanutpanda.quest.gameobjects.MapItem;
import com.badlogic.gdx.graphics.Color;

public class CampScreen extends GameScreen {

    MapInventoryItem tomatoInMap;

    public CampScreen(Quest parent, String mapLoc) {
        super(parent, mapLoc);

        MapItem sausagesItem = parent.createMapItem("sausages_mapItem", tiledMap);
        MapItem campfireItem = parent.createMapItem("campfire_mapItem", tiledMap);
        MapItem tentItem1 = parent.createMapItem("tent2_mapItem", tiledMap);
        MapItem tentItem2 = parent.createMapItem("tent1_mapItem", tiledMap);

        tomatoInMap = parent.createMapInventoryItem("tomato_mapInvItem", tiledMap);

        AnimatedItem fireAnim = parent.createAnimatedItem("fireAnim", tiledMap);

        mapItems.addAll(sausagesItem, campfireItem, tentItem1, tentItem2);
        mapInventoryItems.addAll(tomatoInMap);
        animatedItems.add(fireAnim);

        initExitButtons();
        initTransparantMapButtons();

        this.lightRenderer.createLightPoint(
                campfireItem.centerX, campfireItem.centerY,
                new Color(1.0f, 0.9f, 0.0f, 1.0f), 100, true);


    }

    @Override
    public void initLayers() {
        layerBackground = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
        layerAfterBackground = new int[]{};
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        super.render(delta);

        stage.getBatch().begin();

        if (tomatoInMap.isTaken()) {
            moveItemToInventory(tomatoInMap, parent.itemHandler.tomato);

        }
        printCursorText();

        stage.getBatch().end();

        if (parent.itemHandler.tomato.isUsedCorrectly()) {
            parent.itemHandler.tomato.setName("Potato");
        }

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
