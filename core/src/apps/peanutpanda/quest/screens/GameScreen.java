package apps.peanutpanda.quest.screens;

import apps.peanutpanda.quest.CommandStateHandler;
import apps.peanutpanda.quest.ExitMap;
import apps.peanutpanda.quest.Quest;
import apps.peanutpanda.quest.controller.Mouse;
import apps.peanutpanda.quest.gameobjects.*;
import apps.peanutpanda.quest.light.Clock;
import apps.peanutpanda.quest.light.LightRenderer;
import apps.peanutpanda.quest.maphandling.MapBleedFixer;
import apps.peanutpanda.quest.maphandling.MapLayerHandler;
import apps.peanutpanda.quest.states.*;
import apps.peanutpanda.quest.ui.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.LinkedHashMap;
import java.util.Map;

@SuppressWarnings("LibGDXUnsafeIterator")
public abstract class GameScreen implements Screen, MapLayerHandler {

    Quest parent;
    TiledMap tiledMap;
    private Flag flag;
    int[] layerBackground;
    int[] layerAfterBackground;
    Array<MapItem> mapItems;
    Array<MapInventoryItem> mapInventoryItems;
    Array<AnimatedItem> animatedItems;
    Array<NPC> npcs;
    private Array<Array<? extends Entity>> allEntitiesOnScreen;

    Map<ExitCursorDirection, ExitMap> exitMap;

    private OrthographicCamera camera;
    private Viewport viewport;
    private OrthogonalTiledMapRenderer tiledMapRenderer;
    private BitmapFont fontHoverText;
    private float elapsedTime = 0f;

    Stage stage;
    private Table verbsTable;

    GameState gameState;

    Array<SceneryItemButton> npcBtns;
    private Array<ExitButton> exitBtns;
    private Sprite background;

    private Command command;
    private Inventory inventory;

    private VerbGrid verbGrid;
    private ConversationGrid conversationGrid;

    private OutputDialog outputDialog;

    private GlyphLayout glyphLayout;

    private Array<String> conversationSentences;
    private Array<String> responses;

    private boolean verbsAdded, sentencesAdded;

    private SpeakerState speakerState;
    LightRenderer lightRenderer;

    private Clock clock;
    private float rotateTime = 0;
    float expandTime = 0;

    private boolean exitClicked;
    private ScreenState screenStateForExit;

    private Mouse mouse;
    private CommandStateHandler commandStateHandler;


    GameScreen(Quest parent, String mapLoc) {
        this.parent = parent;
        this.stage = parent.stage;
        this.stage = parent.stage;
        this.command = parent.command;
        this.npcBtns = new Array<>();
        this.fontHoverText = parent.fontHoverText;
        this.camera = parent.camera;
        this.viewport = parent.viewport;
        this.camera.update();
        this.mapItems = new Array<>();
        this.mapInventoryItems = new Array<>();
        this.animatedItems = new Array<>();
        this.npcs = new Array<>();
        this.allEntitiesOnScreen = new Array<>();
        this.exitMap = new LinkedHashMap<>();
        this.exitBtns = new Array<>();
        this.inventory = parent.inventory;
        this.verbGrid = parent.verbGrid;
        this.conversationGrid = parent.conversationGrid;
        this.outputDialog = parent.outputDialog;

        this.tiledMap = parent.assLoader.manager.get(mapLoc);

        this.gameState = parent.gameState;
        this.flag = parent.flag;
        this.conversationSentences = new Array<>();
        this.responses = new Array<>();

        MapBleedFixer.fixTiledMapBleed(tiledMap);
        this.tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap);
        this.initLayers();
        this.background = new Sprite(parent.assLoader.blackBackground());
        this.background.setBounds(0, 0, ScreenSize.WIDTH.getSize(), 68);
        this.background.setAlpha(0.2f);

        Table rootTable = new Table();
        verbsTable = new Table();

        rootTable.setFillParent(true);
        rootTable.bottom().left();

        for (int i = 0; i < inventory.getItems().size; i++) {
            inventory.getItems().get(i).setBounds(parent.invBtnBackground.get(i));
        }

        addVerbsToTable();
        rootTable.add(verbsTable);
        stage.addActor(rootTable);

        parent.assLoader.starDew().setLooping(true);
        parent.assLoader.starDew().play();

        glyphLayout = new GlyphLayout(fontHoverText, "");
        speakerState = SpeakerState.PLAYER;

        this.allEntitiesOnScreen.add(inventory.getItems(), mapInventoryItems, npcs, mapItems); //alle Entities voor Command te genereren

        this.lightRenderer = parent.lightRenderer;
        this.clock = parent.clock;

        this.exitClicked = false;
        this.mouse = parent.mouse;

        this.commandStateHandler = new CommandStateHandler(command);
    }

    void moveItemToInventory(MapInventoryItem mapInventoryItem, InventoryItem inventoryItem) { // remove mapInvItem van map, add invItem
        if (mapInventoryItems.contains(mapInventoryItem, true)) {
            mapInventoryItems.removeValue(mapInventoryItem, true);
            inventory.add(inventoryItem);
        }
    }

    private void addVerbsToTable() {

        if (!verbsAdded) {
            verbGrid.addVerbsToTable(verbsTable);
            verbsAdded = true;
            sentencesAdded = false;

        }
    }

    private void addSentencesToTable() {

        if (!sentencesAdded) {
            conversationGrid.addSentencesToTable(verbsTable, conversationSentences);
            sentencesAdded = true;
            verbsAdded = false;
        }
    }

    private void showFPS() {
//        fontHoverText.draw(stage.getBatch(), "X : " + mousePos.x + " Y : " + mousePos.y, 5, stage.getHeight() - 5);
        fontHoverText.draw(stage.getBatch(), clock.time24h, stage.getWidth() - 58, stage.getHeight() - 65);
        fontHoverText.draw(stage.getBatch(), clock.timeAmPm, stage.getWidth() - 58, stage.getHeight() - 80);
        fontHoverText.draw(stage.getBatch(), "FPS : " + Gdx.graphics.getFramesPerSecond(), 5, stage.getHeight() - 5);
    }

    Rectangle mapExit(String layerName) {
        return ((RectangleMapObject) tiledMap.getLayers()
                .get(layerName).getObjects().get(0))
                .getRectangle();
    }

    void initExitButtons() {

        this.exitBtns = new Array<>();

        for (Map.Entry<ExitCursorDirection, ExitMap> entry : exitMap.entrySet()) {
            exitBtns.add(new ExitButton(entry.getValue().getRectangle(), entry.getValue().getScreenState(), entry.getKey()));
        }

        for (ExitButton btn : exitBtns) {
            stage.addActor(btn);
        }

        for (final ExitButton btn : exitBtns) {
            btn.addListener(new ClickListener() {
                public void clicked(InputEvent event, float x, float y) {
                    if (gameState == GameState.IN_GAME) {
                        exitClicked = true;
                        screenStateForExit = btn.getState();
                    }
                }

                @Override
                public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                    mouse.setExitCursor(btn.getExitCursorDirection());
                }

                @Override
                public void exit(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                    mouse.setDefaultCursor();
                }
            });
        }
    }

    void initTransparantMapButtons() {
        Array<SceneryItemButton> sceneryItemBtns = new Array<>();

        for (MapItem item : mapItems) {
            sceneryItemBtns.add(new SceneryItemButton(item));
        }

        for (MapInventoryItem item : mapInventoryItems) {
            sceneryItemBtns.add(new SceneryItemButton(item));
        }

        for (SceneryItemButton btn : sceneryItemBtns) {
            stage.addActor(btn);
        }

        for (final SceneryItemButton btn : sceneryItemBtns) {
            btn.addListener(new ClickListener() {
                public void clicked(InputEvent event, float x, float y) {
                    command.handle(btn.getItem());
                }
            });
            btn.addListener(new ClickListener(Input.Buttons.RIGHT) {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    command.completeSingleCommand(btn.getItem().findDefaultVerbByName(), btn.getItem());
                }
            });
        }
    }

    void initNpcTransparantMapButtons() {

        for (NPC npc : npcs) {
            npcBtns.add(new SceneryItemButton(npc));
        }

        for (SceneryItemButton btn : npcBtns) {
            stage.addActor(btn);
        }

        for (final SceneryItemButton btn : npcBtns) {
            btn.addListener(new ClickListener() {
                public void clicked(InputEvent event, float x, float y) {
                    command.handle(btn.getItem());
                }
            });
            btn.addListener(new ClickListener(Input.Buttons.RIGHT) {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    command.completeSingleCommand(btn.getItem().findDefaultVerbByName(), btn.getItem());
                }
            });
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, .1f);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        delta = Math.min(delta, 0.1f);

        rotateTime += delta;

        camera.update();
        mouse.update(viewport);

        elapsedTime += delta;

        tiledMapRenderer.setView(camera);
        tiledMapRenderer.render(layerBackground);

        stage.getBatch().begin();
        drawAnimations();
        drawMapInventoryItems();

        renderNPC(delta);
        stage.getBatch().end();

        tiledMapRenderer.render(layerAfterBackground);
        this.lightRenderer.dayNightCycle(clock, parent.zone, delta);
        this.lightRenderer.render();

        stage.getBatch().begin();

        background.draw(stage.getBatch());

        if (gameState != GameState.IN_CONVERSATION) {

            for (Sprite sprite : parent.invBtnBackground) {
                sprite.draw(stage.getBatch());
            }

            for (InventoryItem item : inventory.getItems()) {
                item.getSprite().draw(stage.getBatch());
            }
        } else {
            command.clear();
            if (conversationGrid.getSentenceChosen() != 0) {
                if (conversationGrid.getSentenceChosen() == 4) {
                    leaveConversation();
                } else {
                    conversationGrid.hideSentences();
                    drawConversationDialog(
                            conversationSentences.get(conversationGrid.getSentenceChosen() - 1),
                            responses.get(conversationGrid.getSentenceChosen() - 1),
                            commandStateHandler.getTalkingNpc().getCloseUpSprite());
                }

            } else {
                conversationGrid.revealSentences();
            }
        }

        commandStateHandler.checkCommandState(outputDialog);

        if (commandStateHandler.isConversationEntered()) {
            enterConversation(commandStateHandler.getTalkingNpc());
        }

        if (command.state == SingleCommandState.NONE) {
            createHoverText();
        }

        showFPS();

        clock.drawCycle(delta);

        stage.getBatch().end();

        stage.act();
        stage.draw();

        if (exitClicked) {
            for (ExitButton btn : exitBtns) {
                btn.remove();
            }
            command.clear();
            parent.changeScreen(screenStateForExit);
        }

    }

    private void drawConversationDialog(String textToPrint, String textToPrintResponse, Sprite npcCloseUp) {

        switch (speakerState) {
            case PLAYER:
                outputDialog.drawPlayerDialog(textToPrint);
                if (mouse.isButton1Down() || mouse.isButton2Down()) {
                    mouse.setButton1Down(false);
                    mouse.setButton2Down(false);
                    speakerState = SpeakerState.NPC;
                }
                break;
            case NPC:
                outputDialog.drawNpcDialog(textToPrintResponse, npcCloseUp);
                if (mouse.isButton1Down() || mouse.isButton2Down()) {
                    conversationGrid.setSentenceChosen(0);
                    speakerState = SpeakerState.PLAYER;
                }
                break;
        }
    }

    private void enterConversation(NPC npc) {
        this.background.setAlpha(0.65f);
        Array<String> npcSentences = npc.getConversations();
        Array<String> npcResponses = npc.getResponses();
        for (int i = 0; i < npcSentences.size; i++) {
            if (this.conversationSentences.size < npcSentences.size) {
                this.conversationSentences.addAll(npcSentences.get(i));
            }
            if (this.responses.size < npcResponses.size) {
                this.responses.addAll(npcResponses.get(i));
            }
        }
        command.clear();
        gameState = GameState.IN_CONVERSATION;
        addSentencesToTable();
    }

    private void leaveConversation() {
        commandStateHandler.setConversationEntered(false);
        conversationGrid.hideSentences();
        outputDialog.drawPlayerDialog(conversationSentences.get(conversationGrid.getSentenceChosen() - 1));
        if (mouse.isButton1Down() || mouse.isButton2Down()) {
            gameState = GameState.IN_GAME;
            addVerbsToTable();
            conversationGrid.setSentenceChosen(0);
        }
    }

    private void renderNPC(float delta) {
        for (NPC npc : npcs) {
            npc.move(delta, stage, gameState);
        }
    }

    private void drawAnimations() {
        for (AnimatedItem item : animatedItems) {
            item.animate(stage, elapsedTime);
        }
    }

    private void drawMapInventoryItems() {
        for (MapInventoryItem item : mapInventoryItems) {
            if (!item.isTaken()) {
                stage.getBatch().draw(item.getTexture(), item.getRect().x, item.getRect().y, item.getWidth(), item.getHeight());
            }
        }
    }

    void printCursorText() {
        glyphLayout.setText(fontHoverText, command.printComplete());
        float posY = (mouse.getPosition().y >= stage.getHeight() - 25) ? -12 : 25;

        float posX;

        if (mouse.getPosition().x <= glyphLayout.width - 55) {
            posX = 4;
        } else if (mouse.getPosition().x + glyphLayout.width / 2 > ScreenSize.WIDTH.getSize()) {
            posX = (mouse.getPosition().x - glyphLayout.width) + 10;
        } else {
            posX = ((mouse.getPosition().x + 4) - glyphLayout.width / 2);
        }

        fontHoverText.draw(stage.getBatch(), glyphLayout, posX, (mouse.getPosition().y + posY));
    }

    private void createHoverText() {

        for (Sprite sprite : parent.invBtnBackground) {
            if (sprite.getBoundingRectangle().contains(mouse.getPosition().x, mouse.getPosition().y)) {
                sprite.setAlpha(0.4f);
            } else {
                sprite.setAlpha(parent.invBtnBackgrAlpha);
            }
        }

        for (Array<? extends Entity> array : allEntitiesOnScreen) {
            for (Entity item : array) {
                if (item.getRect().contains(mouse.getPosition().x, mouse.getPosition().y)) {

                    item.findDefaultVerbButton(verbGrid).setChecked(true);

                    if (!command.isWaitingToUse()) {
                        command.setItem(item);
                        return;
                    } else {
                        command.setItem2(item);
                        return;
                    }
                } else {
                    item.findDefaultVerbButton(verbGrid).setChecked(false);
                    if (!command.isWaitingToUse()) {
                        command.removeItem();
                    } else {
                        command.removeItem2();
                    }
                }
            }
        }
    }


    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        lightRenderer.resize(viewport);
    }

    @Override
    public void dispose() {
        lightRenderer.dispose();
        parent.assLoader.manager.dispose();
    }
}
