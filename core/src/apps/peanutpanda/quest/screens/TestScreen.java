package apps.peanutpanda.quest.screens;

import apps.peanutpanda.quest.Quest;

public class TestScreen extends GameScreen {

    public TestScreen(Quest parent, String mapLoc) {
        super(parent, mapLoc);
    }

    @Override
    public void initLayers() {
        layerBackground = new int[]{0};
        layerAfterBackground = new int[]{};
    }


    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        super.render(delta);

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
