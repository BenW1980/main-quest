package apps.peanutpanda.quest.screens;

import apps.peanutpanda.quest.ExitMap;
import apps.peanutpanda.quest.Quest;
import apps.peanutpanda.quest.ai.PatrolHorizBetweenCoords;
import apps.peanutpanda.quest.gameobjects.MapItem;
import apps.peanutpanda.quest.gameobjects.NPC;
import apps.peanutpanda.quest.states.ExitCursorDirection;
import apps.peanutpanda.quest.states.GameState;
import apps.peanutpanda.quest.states.ScreenState;

public class FarmScreen extends GameScreen {

    private NPC farmer;

    public FarmScreen(Quest parent, String mapLoc) {
        super(parent, mapLoc);

        farmer = parent.createNPC("farmer", 220, 120);
        farmer.setMovementStrategy(new PatrolHorizBetweenCoords(farmer, 200, 250));
        npcs.add(farmer);

        initNpcTransparantMapButtons();

        MapItem axe = parent.createMapItem("axe_mapItem", tiledMap);
        MapItem corn = parent.createMapItem("corn_mapItem", tiledMap);
        MapItem scarecrow = parent.createMapItem("scarecrow_mapItem", tiledMap);
        MapItem seeds = parent.createMapItem("seeds_mapItem", tiledMap);
        MapItem carrots = parent.createMapItem("carrots_mapItem", tiledMap);

        mapItems.addAll(axe, corn, scarecrow, seeds, carrots);
        exitMap.put(ExitCursorDirection.RIGHT, new ExitMap(mapExit("exit_east"), ScreenState.OUTSIDEHOME));
        initExitButtons();
        initTransparantMapButtons();
    }

    @Override
    public void initLayers() {
        layerBackground = new int[]{0, 1, 2, 3, 5, 6, 8, 9, 10, 11, 12, 13, 15, 16};
        layerAfterBackground = new int[]{4, 7, 14, 17, 18};
    }


    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        super.render(delta);

        stage.getBatch().begin();
        if (gameState != GameState.IN_CONVERSATION) {
            printCursorText();
        }
        stage.getBatch().end();

        for (int i = 0; i < npcs.size; i++) {
            npcBtns.get(i).setPosition(npcs.get(i).getRect().x, npcs.get(i).getRect().y);
        }

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
