package apps.peanutpanda.quest.screens;

import apps.peanutpanda.quest.ExitMap;
import apps.peanutpanda.quest.Quest;
import apps.peanutpanda.quest.states.ExitCursorDirection;
import apps.peanutpanda.quest.states.ScreenState;

public class OutsidePlayerHomeScreen extends GameScreen {


    public OutsidePlayerHomeScreen(Quest parent, String mapLoc) {
        super(parent, mapLoc);

        exitMap.put(ExitCursorDirection.LEFT, new ExitMap(mapExit("exit_west"), ScreenState.FARM));
        exitMap.put(ExitCursorDirection.UP, new ExitMap(mapExit("exit_in"), ScreenState.HOME));
        initExitButtons();
        initTransparantMapButtons();

    }

    @Override
    public void initLayers() {
        layerBackground = new int[]{0, 1, 2, 10};
        layerAfterBackground = new int[]{3, 4, 5, 6, 7, 8, 9, 11, 12};
    }


    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        super.render(delta);

        stage.getBatch().begin();
        printCursorText();
        stage.getBatch().end();

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
