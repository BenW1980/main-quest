package apps.peanutpanda.quest.screens;

import apps.peanutpanda.quest.ExitMap;
import apps.peanutpanda.quest.Quest;
import apps.peanutpanda.quest.gameobjects.AnimatedItem;
import apps.peanutpanda.quest.gameobjects.MapItem;
import apps.peanutpanda.quest.states.ExitCursorDirection;
import apps.peanutpanda.quest.states.ScreenState;
import com.badlogic.gdx.graphics.Color;

public class PlayerHomeScreen extends GameScreen {

    public PlayerHomeScreen(Quest parent, String mapLoc) {
        super(parent, mapLoc);

        MapItem fireplaceItem = parent.createMapItem("fireHome_mapItem", tiledMap);
        MapItem tableItem = parent.createMapItem("tableHome_mapItem", tiledMap);
        MapItem bedItem = parent.createMapItem("bedHome_mapItem", tiledMap);
        AnimatedItem fireAnim = parent.createAnimatedItem("fireAnim", tiledMap);

        mapItems.addAll(fireplaceItem, tableItem, bedItem);
        animatedItems.addAll(fireAnim);


        exitMap.put(ExitCursorDirection.DOWN, new ExitMap(mapExit("exit_out"), ScreenState.OUTSIDEHOME));
        initExitButtons();
        initTransparantMapButtons();

        this.lightRenderer.createLightPoint(
                fireplaceItem.centerX, fireplaceItem.centerY, new Color(1.0f, 0.4f, 0.05f, 1.0f), 100, true);
        this.lightRenderer.createLightPoint(236.0f, 245.0f, new Color(1.0f, 0.4f, 0.05f, 1.0f), 40, false);
        this.lightRenderer.createLightPoint(242.0f, 181.0f, new Color(1.0f, 0.4f, 0.05f, 1.0f), 40, false);
        this.lightRenderer.createLightPoint(361.0f, 151.0f, new Color(1.0f, 0.4f, 0.05f, 1.0f), 40, false);
        this.lightRenderer.createLightPoint(156.0f, 135.0f, new Color(1.0f, 0.4f, 0.05f, 1.0f), 100, false);
    }

    @Override
    public void initLayers() {
        layerBackground = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13};
        layerAfterBackground = new int[]{12};
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        super.render(delta);

        stage.getBatch().begin();
        printCursorText();
        stage.getBatch().end();

        this.lightRenderer.setAmbientInside();

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();

    }
}