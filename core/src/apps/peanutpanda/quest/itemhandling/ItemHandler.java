package apps.peanutpanda.quest.itemhandling;

import apps.peanutpanda.quest.assetmanager.AssetLoader;
import apps.peanutpanda.quest.gameobjects.Entity;
import apps.peanutpanda.quest.gameobjects.InventoryItem;
import com.badlogic.gdx.utils.Array;

public class ItemHandler {

    private Array<Array<? extends Entity>> allEntitiesFromJson;
    public InventoryItem book;
    public InventoryItem torch;
    public InventoryItem tomato;

    public ItemHandler(AssetLoader assLoader, Array<Array<? extends Entity>> allEntitiesFromJson) {
        this.allEntitiesFromJson = allEntitiesFromJson;
        book = new InventoryItem("Book", assLoader.book());
        torch = new InventoryItem("Torch", assLoader.fire());
        tomato = new InventoryItem("Tomato", assLoader.tomato());
        setUses();
    }


    private void setUses() {
        torch.setResponseOnWrongUsage("I don't want to burn that.");
        tomato.setUseWithItem(findEntityById("campfire_mapItem"));
        torch.setUseWithItem(book);
        torch.setResponseOnCorrectUsage("Light!");

    }

    private Entity findEntityById(String id) {
        Entity entity = null;
        for (Array<? extends Entity> entityArray : allEntitiesFromJson) {
            for (Entity e : entityArray) {
                if (e.getId().toLowerCase().equals(id.toLowerCase())) {
                    entity = e;
                }
            }
        }
        return entity;
    }
}